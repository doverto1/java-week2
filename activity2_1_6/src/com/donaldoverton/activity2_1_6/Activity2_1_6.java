package com.donaldoverton.activity2_1_6;
// Assignment: Activity2_1_6
// File Name: activity2_1_6.java
// Name: Donald Overton
// StudentID: 10000537505
// Topic: Use a sentinel
// Description: Write a program that reads and calculates the sum of an unspecified number of integers.


import java.util.Scanner;

public class Activity2_1_6 {
	public static void main(String[] args)
	{
		//initialize the sum variable to store the total
		int sum = 0;

		//initialize a scanner
		Scanner scan = new Scanner(System.in);

		//prompt user for number
		System.out.print("Enter an integer (the input ends if it is 0): ");

		//assign the input to a variable that will be used as a sentinel.
		int data = scan.nextInt();

		//while loop that will keep accepting user input as long as it does not equal the sentinel
		while(data != 0 ){
			sum += data;
			//prompt user for number
			System.out.print("Enter an integer (the input ends if it is 0): ");
			data = scan.nextInt();
		}
		System.out.println("The sum is " + sum);

	}
}
