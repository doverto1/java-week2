package com.donaldoverton.activity2_1_2;
import java.util.Scanner;

// Assignment: Activity2_1_2
// File Name: Activity_2_1_2.java
// Name: Donald Overton
// StudentID: 10000537505
// Topic: Two way if statement
// Description: prompt user to enter a decimal number representing the radius of a circle.

public class Activity2_1_2 {
	public static void main(String[] args)
	{
		double PI = 3.14;
		double area;

		//Create new scanner to get user input
		Scanner scan = new Scanner(System.in);

		//Get the user input
		System.out.print("Enter the radius: ");
		double radius = scan.nextDouble();


		//if input is less than 0 let the user know that the input is negative
		if(radius < 0){
			System.out.println("Negative input");
		} else {
			//Calculate the area for the given radius
			area = PI * radius * radius;

			System.out.println("The area for the circle of radius "+radius+" is "+area);
		}


	}

}
