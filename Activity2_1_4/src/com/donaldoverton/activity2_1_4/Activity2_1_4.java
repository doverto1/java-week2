package com.donaldoverton.activity2_1_4;
// Assignment: Activity2.1.4
// File Name: Activity2_1_4
// Name: Donald Overton
// StudentID: 10000537505
// Topic: Switch Statement
// Description: Switch statement outputs the month.

import java.util.Scanner;

public class Activity2_1_4 {
	public static void main(String[] args)
	{
		String message;

		//create scanner and ask for user inputted month
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter an integer corresponding to a month: ");
		int userSuppliedMonth = scan.nextInt();

		//crate switch statement for user supplied month
		switch (userSuppliedMonth){
			case 1:
				message = "The month is January.";
				break;

			case 2:
				message = "The month is February.";
				break;

			case 3:
				message = "The month is March.";
				break;

			case 4:
				message = "The month is April.";
				break;

			case 5:
				message = "The month is May.";
				break;

			case 6:
				message = "The month is June.";
				break;

			case 7:
				message = "Them month is July.";
				break;

			case 8:
				message = "The month is August.";
				break;

			case 9:
				message = "The month is September.";
				break;

			case 10:
				message = "The month is October.";
				break;

			case 11:
				message = "The month is November.";
				break;

			case 12:
				message = "The month is December.";
				break;

			default:
				message = "Wrong input.";

		}

		//output message to the user
		System.out.println(message);
	}
}
