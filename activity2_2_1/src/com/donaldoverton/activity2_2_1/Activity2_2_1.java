package com.donaldoverton.activity2_2_1;
 // Assignment: Activity2_2_1
 // File Name: Activity2_2_1.java
 // Name: Donald Overton
 // StudentID: 10000537505
 // Topic: Arrays
 // Description: Write a program that finds the number of items above the average of all items.


import java.util.Scanner;

public class Activity2_2_1 {
	public static void main(String[] args)
	{
		double average = 0;
		int numberOfElementsAboveAverge = 0;
		double sum = 0;

		Scanner scan = new Scanner(System.in);
		//prompt user
		System.out.print("Enter the number of items:" );
		int numberOfItems = scan.nextInt();

		double[] numberList = new double[numberOfItems];

		System.out.print("Enter the numbers: ");

		//populate the array
		for(int i=0; i < numberList.length;i++){
			double number = scan.nextDouble();
			numberList[i] = number;
			sum += number;

		}

		//compute the average
		average = sum / numberOfItems;
		System.out.println("Average is: " +average);

		// check if the elements in the array are above the average
		for(int x=0; x < numberList.length; x++){
			if(numberList[x] > average){
				numberOfElementsAboveAverge++;
			}
		}

		System.out.println("Number of elements above average is: " + numberOfElementsAboveAverge);

	}
}
