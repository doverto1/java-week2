package com.donaldoverton.activity2_1_3;

// Assignment: Activity 2-1-3
// File Name: Activity2_1_3
// Name: Donald Overton
// StudentID: 10000537505
// Topic: Logical Operators
// Description: Program takes in an input year an figures out if it is a leap year.

import java.util.Scanner;

public class Activity2_1_3 {
	
	public static void main(String[] args)
	{
		//prompt user for the input
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter a year: ");

		int userSuppliedYear = scan.nextInt();

		//if userSuppliedYear is divisible by 4 but not 100 or it is divisible by 400

		if(((userSuppliedYear % 4 == 0) && (userSuppliedYear % 100 !=0)) || userSuppliedYear % 400 == 0)
		{
			System.out.println(userSuppliedYear+ " is a leap year? true");
		}
		else
		{
			System.out.println(userSuppliedYear+ " is a leap year? false");
		}
	}
}
