package com.donaldoverton.activity2_1_5;
// Assignment: Activity2_1_5
// File Name: Activity2_1_5.java
// Name: Donald Overton
// StudentID: 10000537505
// Topic: While loop
// Description: Write a program that prompts the user to enter an answer for a question on addition of two single digits.

import java.util.Scanner;

public class Activity2_1_5 {
	public static void main(String[] args)
	{
		//prompt user for input
		Scanner scan = new Scanner(System.in);

		System.out.print("What is 20+22? ");
		int userSuppliedInt = scan.nextInt();

		//keep prompting the user until the input is 42
		while(userSuppliedInt != 42)
		{
			System.out.println("Wrong Answer. Try again.");
			System.out.print("What is 20+22? ");
			userSuppliedInt = scan.nextInt();
		}
		System.out.println("You got it!");
	}
}
