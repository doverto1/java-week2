package com.donaldoverton.activity2_1_8;

// Assignment: Activity2_1_8
// File Name: Activity2_1_8.java
// Name: Donald Overton
// StudentID: 10000537505
// Topic: For Loop
// Description: Write a for loop that reads an integer from the user, and prints the sum of numbers from 1 to that integer

import java.util.Scanner;

public class Activity2_1_8 {
	public static void main(String[] args)
	{
		int sum = 0;
		//create a scanner
		Scanner scan = new Scanner(System.in);

		//prompt user for input
		System.out.print("Enter an integer greater or equal to 1: ");
		int data = scan.nextInt();

		//check to see if input is zero or less
		while(data <= 0) {
			//prompt user for input
			System.out.print("Enter an integer greater or equal to 1: ");
			data = scan.nextInt();
		}
		for (int x = 1; x <= data; x++) {
			//add the value of x to sum
			sum = x+sum;
		}

		System.out.println("The sum of integers from 1 to "+data+ " is "+sum);
	}
}
