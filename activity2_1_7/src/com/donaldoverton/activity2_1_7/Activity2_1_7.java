package com.donaldoverton.activity2_1_7;

// Assignment: Activity2_1_7
// File Name: activity2_1_7.java
// Name: Donald Overton
// StudentID: 10000537505
// Topic: Do while loop
// Description: Writes a program that reads and calculates the sum of an unspecified number of integers.

import java.util.Scanner;

public class Activity2_1_7 {
	public static void main(String[] args)
	{
		//initialize sum variable
		int sum = 0;
		int data;
		//initialize the scanner for user input
		Scanner scan = new Scanner(System.in);

		//create do while loop that will prompt user
		do{
			System.out.print("Enter an integer (the input ends if it is 0): ");
			data = scan.nextInt();
			sum += data;

		}while (data != 0);
		System.out.println("The sum is " +sum);
	}
}
