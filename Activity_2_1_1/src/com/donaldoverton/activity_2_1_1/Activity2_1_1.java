package com.donaldoverton.activity_2_1_1;

// Assignment: Example 2-1.1
// File Name: Activity2_1_1.java
// Name: Donald Overton
// StudentID: 10000537505
// Topic: Control Flow If Statement
// Description: Write a program that prompt the user to enter an integer.


import java.util.Scanner;

public class Activity2_1_1 {

	public static void main(String[] args)
	{
		Scanner scan = new Scanner(System.in);

		//prompt user
		System.out.print("Enter an integer: ");
		//get the user input
		int userSuppliedNumber = scan.nextInt();

		//if the number is divisible by 5
		if(userSuppliedNumber % 5 == 0){
			System.out.println("HiFive");
		}
		//if the number is divisible by 2
		if(userSuppliedNumber % 2 == 0){
			System.out.println("HiEven");
		}

	}

}